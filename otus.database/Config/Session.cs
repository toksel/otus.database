﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Context;
using NHibernate.Mapping.Attributes;
using NHibernate.Tool.hbm2ddl;
using System.Collections.Generic;
using System.Reflection;

namespace otus.database
{
   static class Session
   {
      public static ISessionFactory BuildSessionFactory(string connectionString, bool printLog = false)
      {
         var configProperties = new Dictionary<string, string>
         {
            { Environment.ConnectionDriver, typeof(NHibernate.Driver.NpgsqlDriver).FullName },
            { Environment.Dialect         , typeof(NHibernate.Dialect.PostgreSQL83Dialect).FullName },
            { Environment.ConnectionString, connectionString },
         };

         if (printLog)
         {
            configProperties.Add("show_sql", "true");
         }

         var serializer = HbmSerializer.Default;
         serializer.Validate = true;

         var configuration = new Configuration()
            .SetNamingStrategy(new PgSqlNamingStategy())
            .SetProperties(configProperties)
            .AddInputStream(serializer.Serialize(Assembly.GetExecutingAssembly()));

         new SchemaUpdate(configuration).Execute(true, true);

         return configuration.BuildSessionFactory();
      }
   }
}
