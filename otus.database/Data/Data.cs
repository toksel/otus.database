﻿using System.Collections.Generic;
using System.Linq;

namespace otus.database
{
   static class Data
   {
      public static List<CourseEntity> GetCourseData()
      {
         return new List<CourseEntity>()
         {
            new CourseEntity()
            {
               Name = "C# Developer. Professional",
               Description = "Best Practice по разработке на C# и .NET Framework с практикой Scrum-методики"
            },
            new CourseEntity()
            {
               Name = "PostgreSQL",
               Description = "Полный курс по работе с базой данных PostgreSQL"
            },
            new CourseEntity()
            {
               Name = "Разработчик IoT",
               Description = "Стань востребованным специалистом в сфере интернета вещей!"
            },
            new CourseEntity()
            {
               Name = "Архитектура и шаблоны проектирования",
               Description = "Для разработчиков, которые хотят изучить основные паттерны проектирования и научиться применять их, находить им замену в сложных ситуация и научиться мыслить, как архитектор программного обеспечения"
            },
            new CourseEntity()
            {
               Name = "Алгоритмы и структуры данных",
               Description = "Практический курс, который поможет развить алгоритмическое мышление и повысить производительность своих программ"
            },
         };
      }

      public static List<LessonEntity> GetLessonsData()
      {
         CourseEntity course = GetCourseData().Where(w => w.Name == "C# Developer. Professional").FirstOrDefault();
         return new List<LessonEntity>()
         {
            new LessonEntity()
            {
               Course = course,
               Name = "Знакомство",
               Description = "Знакомство, рассказ о формате Scrum, краткий обзор курса"
            },
            new LessonEntity()
            {
               Course = course,
               Name = "Архитектура проекта",
               Description = "Рассмотреть возможные варианты архитектуры проекта"
            },
            new LessonEntity()
            {
               Course = course,
               Name = "Операторы и методы, их перегрузка и расширения",
               Description = "Рассмотреть операторы и методы, их перегрузка и расширения"
            },
            new LessonEntity()
            {
               Course = course,
               Name = "Интерфейсы и их особенности",
               Description = "Рассмотерть интерфейсы и их особенности"
            },
            new LessonEntity()
            {
               Course = course,
               Name = "Особенности встроенных коллекций",
               Description = "Рассмотреть особенности встроенных коллекций"
            },
         };
      }

      public static List<StudentEntity> GetStudentsData()
      {
         CourseEntity course = GetCourseData().Where(w => w.Name == "C# Developer. Professional").FirstOrDefault();
         return new List<StudentEntity>()
         {
            new StudentEntity()
            {
               Course = course,
               FirstName = "Student1",
               LastName = "Tneduts1",
               Email = "student1@mail.ru"
            },
            new StudentEntity()
            {
               Course = course,
               FirstName = "Student2",
               LastName = "Tneduts2",
               Email = "student2@mail.ru"
            },
            new StudentEntity()
            {
               Course = course,
               FirstName = "Student3",
               LastName = "Tneduts3",
               Email = "student3@mail.ru"
            },
            new StudentEntity()
            {
               Course = course,
               FirstName = "Student4",
               LastName = "Tneduts4",
               Email = "student4@mail.ru"
            },
            new StudentEntity()
            {
               Course = course,
               FirstName = "Student5",
               LastName = "Tneduts5",
               Email = "student5@mail.ru"
            },
         };
      }
   }
}
