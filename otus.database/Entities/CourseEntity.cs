﻿using NHibernate.Mapping.Attributes;
using System.Collections.Generic;

namespace otus.database
{
   [Class(Table = "courses")]
   class CourseEntity : IHasId
   {
      [Id(0, Name = "Id")]
      [Generator(1, Class = "native")]
      public virtual long Id { get; set; }

      [Property(NotNull = true, Unique = true, UniqueKey = "uc_course_name")]
      public virtual string Name { get; set; }

      [Property]
      public virtual string Description { get; set; }

      [Bag(0, Name = "Students", Inverse = true)]
      [Key(1, Column = "CourseId")]
      [OneToMany(2, ClassType = typeof(StudentEntity))]
      public virtual IList<StudentEntity> Students { get; set; }

      [Bag(0, Name = "Lessons", Inverse = true)]
      [Key(1, Column = "CourseId")]
      [OneToMany(2, ClassType = typeof(LessonEntity))]
      public virtual IList<LessonEntity> Lessons { get; set; }

      public static bool operator ==(CourseEntity a, CourseEntity b)
         => ((object)a) != null && ((object)b) != null && a.Name == b.Name;

      public static bool operator !=(CourseEntity a, CourseEntity b)
         => !(a == b);

      public override int GetHashCode()
         => Name.GetHashCode();

      public override bool Equals(object obj)
         => obj != null &&
            obj.GetType() == typeof(CourseEntity) &&
            this == (CourseEntity)obj;

      public override string ToString()
         => $"Id: {Id}, Name: {Name}, Description: {Description}";
   }
}
