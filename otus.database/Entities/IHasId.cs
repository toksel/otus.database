﻿namespace otus.database
{
   interface IHasId
   {
      public long Id { get; set; }
   }
}
