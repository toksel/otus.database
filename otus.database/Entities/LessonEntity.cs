﻿using NHibernate.Mapping.Attributes;

namespace otus.database
{
   [Class(Table = "lessons")]
   class LessonEntity : IHasId
   {
      [Id(0, Name = "Id")]
      [Generator(1, Class = "native")]
      public virtual long Id { get; set; }

      [ManyToOne(Column = "CourseId", ForeignKey = "fk_lessons_course_id", Cascade = "all")]
      public virtual CourseEntity Course { get; set; }

      [Property(NotNull = true, Unique = true, UniqueKey = "uc_lesson_name")]
      public virtual string Name { get; set; }

      [Property]
      public virtual string Description { get; set; }

      public static bool operator ==(LessonEntity a, LessonEntity b)
         => ((object)a) != null && ((object)b) != null && a.Name == b.Name;

      public static bool operator !=(LessonEntity a, LessonEntity b)
         => !(a == b);

      public override int GetHashCode()
         => Name.GetHashCode();

      public override bool Equals(object obj)
         => obj != null &&
            obj.GetType() == typeof(LessonEntity) &&
            this == (LessonEntity)obj;

      public override string ToString()
         => $"Id: {Id}, Name: {Name}, Description: {Description}, Course {Course.Id}";
   }
}
