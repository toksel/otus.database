﻿using NHibernate.Mapping.Attributes;

namespace otus.database
{
   [Class(Table = "students")]
   class StudentEntity : IHasId
   {
      [Id(0, Name = "Id")]
      [Generator(1, Class = "native")]
      public virtual long Id { get; set; }

      [ManyToOne(Column = "CourseId", ForeignKey = "fk_students_course_id", Cascade = "all")]
      public virtual CourseEntity Course { get; set; }

      [Property(NotNull = true)]
      public virtual string FirstName { get; set; }

      [Property(NotNull = true)]
      public virtual string LastName { get; set; }

      [Property]
      public virtual int Age { get; set; }
      
      [Property(NotNull = true, Unique = true, UniqueKey = "uc_students_email")]
      public virtual string Email { get; set; }

      public static bool operator ==(StudentEntity a, StudentEntity b)
         => ((object)a) != null && ((object)b) != null && a.Email == b.Email;

      public static bool operator !=(StudentEntity a, StudentEntity b)
         => !(a == b);

      public override int GetHashCode()
         => Email.GetHashCode();

      public override bool Equals(object obj)
         => obj != null &&
            obj.GetType() == typeof(StudentEntity) &&
            this == (StudentEntity)obj;

      public override string ToString()
         => $"Id: {Id}, Name: {FirstName} {LastName}, Description: {Email}, Course {Course.Id}";
   }
}
