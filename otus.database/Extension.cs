﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace otus.database
{
   public static class Extension
   {
      public static string ToString<T>(this IList<T> entities)
      {
         StringBuilder sb = new StringBuilder();
         entities.ToList().ForEach(f => sb.AppendLine(f.ToString()));
         return sb.ToString();
      }
   }
}
