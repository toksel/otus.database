﻿using NHibernate;
using System;
using System.Linq;

namespace otus.database
{
   class Program
   {
      static void Main(string[] args)
      {
         var connectionstring = "User ID=postgres;Password=password;Host=localhost;Port=5432;Database=otus;";
         ISessionFactory sessionFactory = Session.BuildSessionFactory(connectionstring, printLog: false);

         DataManager manager = new(new NHCourseRepository(sessionFactory),
                                   new NHLessonRepository(sessionFactory),
                                   new NHStudentRepository(sessionFactory));


         manager.Courses.SaveMany(Data.GetCourseData());
         manager.Lessons.SaveMany(Data.GetLessonsData());
         manager.Students.SaveMany(Data.GetStudentsData());

         Console.WriteLine("\nList of courses:");
         manager.Courses.GetAll().ToList().ForEach(f => Console.WriteLine(f.Name));

         Console.WriteLine("\nList of lessons:");
         manager.Lessons.GetAll().ToList().ForEach(f => Console.WriteLine(f.Name));

         Console.WriteLine("\nList of students:");
         manager.Students.GetAll().ToList().ForEach(f => Console.WriteLine($"{f.FirstName} {f.LastName} {f.Email}"));

         Console.WriteLine("Enter table you want to add data in:");
         Console.WriteLine("1 - Courses");
         Console.WriteLine("2 - Lessons");
         Console.WriteLine("3 - Students");
         Console.WriteLine("0 - Exit");

         var input = new UserInput(manager);
         int inputTable = input.GetNumericInput((a) => a > 0 && a < 4);

         switch (inputTable)
         {
            case 1:
               input.InsertCourse();
               break;
            case 2:
               input.InsertLesson();
               break;
            case 3:
               input.InsertStudent();
               break;
            default:
               break;
         }

         Console.ReadKey();
      }
   }
}
