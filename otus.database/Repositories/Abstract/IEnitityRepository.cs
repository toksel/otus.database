﻿using System.Collections.Generic;
using System.Linq;

namespace otus.database
{
   interface IEntityRepository<T> where T : IHasId
   {
      IList<T> GetAll();
      void SaveOne(T entity);

      void SaveMany(IList<T> entity);

      T GetOneById(int id);
      T GetOneByName(string name);
   }
}
