﻿namespace otus.database
{
   class DataManager
   {
      public IEntityRepository<CourseEntity> Courses { get; set; }
      public IEntityRepository<LessonEntity> Lessons { get; set; }
      public IEntityRepository<StudentEntity> Students { get; set; }
      public DataManager(IEntityRepository<CourseEntity> courseRepository,
                         IEntityRepository<LessonEntity> lessonRepository,
                         IEntityRepository<StudentEntity> studentRepository)
      {
         Courses = courseRepository;
         Lessons = lessonRepository;
         Students = studentRepository;
      }
   }
}
