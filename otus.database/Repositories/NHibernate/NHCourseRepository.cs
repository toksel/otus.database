﻿using NHibernate;
using System.Collections.Generic;
using System.Linq;

namespace otus.database
{
   class NHCourseRepository : IEntityRepository<CourseEntity>
   {
      readonly ISessionFactory _sessionFactory;

      public NHCourseRepository(ISessionFactory sessionFactory)
      {
         _sessionFactory = sessionFactory;
      }

      public IList<CourseEntity> GetAll()
      {
         using ISession session = _sessionFactory.OpenSession();
         using ITransaction transaction = session.BeginTransaction();
         return session.Query<CourseEntity>().ToList();
      }

      public CourseEntity GetOneById(int id)
         => GetAll().Where(w => w.Id == id).FirstOrDefault();

      public CourseEntity GetOneByName(string name)
         => GetAll().Where(w => w.Name == name).FirstOrDefault();

      public void SaveMany(IList<CourseEntity> entities)
      {
         using ISession session = _sessionFactory.OpenSession();
         using ITransaction transaction = session.BeginTransaction();
         foreach (var course in entities)
         {
            if (!session.Query<CourseEntity>().Where(w => w.Name == course.Name).Any())
            {
               session.SaveOrUpdate(course);
            }
         }
         transaction.Commit();
      }

      public void SaveOne(CourseEntity courseEntity)
      {
         using ISession session = _sessionFactory.OpenSession();
         using ITransaction transaction = session.BeginTransaction();
         if (!session.Query<CourseEntity>().Where(w => w.Name == courseEntity.Name).Any())
         {
            session.SaveOrUpdate(courseEntity);
         }
         transaction.Commit();
      }
   }
}
