﻿using NHibernate;
using System.Collections.Generic;
using System.Linq;

namespace otus.database
{
   class NHLessonRepository : IEntityRepository<LessonEntity>
   {
      readonly ISessionFactory _sessionFactory;

      public NHLessonRepository(ISessionFactory sessionFactory)
      {
         _sessionFactory = sessionFactory;
      }

      public IList<LessonEntity> GetAll()
      {
         using ISession session = _sessionFactory.OpenSession();
         using ITransaction transaction = session.BeginTransaction();
         return session.Query<LessonEntity>().ToList();
      }

      public LessonEntity GetOneById(int id)
         => GetAll().Where(w => w.Id == id).FirstOrDefault();

      public LessonEntity GetOneByName(string name)
         => GetAll().Where(w => w.Name == name).FirstOrDefault();

      public void SaveMany(IList<LessonEntity> entities)
      {
         using ISession session = _sessionFactory.OpenSession();
         using ITransaction transaction = session.BeginTransaction();
         foreach (var lesson in entities)
         {
            if (!session.Query<LessonEntity>().Any(a => a.Name == lesson.Name))
            {
               var course = session.Query<CourseEntity>().Where(w => w.Name == lesson.Course.Name).FirstOrDefault();
               if (course != null)
               {
                  lesson.Course = course;
               }
               session.SaveOrUpdate(lesson);
            }
         }
         transaction.Commit();
      }

      public void SaveOne(LessonEntity lessonEntity)
      {
         using ISession session = _sessionFactory.OpenSession();
         using ITransaction transaction = session.BeginTransaction();
         if (!session.Query<LessonEntity>().Any(a => a.Name == lessonEntity.Name))
         {
            var course = session.Query<CourseEntity>().Where(w => w.Name == lessonEntity.Course.Name).FirstOrDefault();
            if (course != null)
            {
               lessonEntity.Course = course;
            }
            session.SaveOrUpdate(lessonEntity);
         }
         transaction.Commit();
      }
   }
}
