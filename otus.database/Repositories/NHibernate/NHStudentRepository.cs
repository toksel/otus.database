﻿using NHibernate;
using System.Collections.Generic;
using System.Linq;

namespace otus.database
{
   class NHStudentRepository : IEntityRepository<StudentEntity>
   {
      readonly ISessionFactory _sessionFactory;

      public NHStudentRepository(ISessionFactory sessionFactory)
      {
         _sessionFactory = sessionFactory;
      }

      public IList<StudentEntity> GetAll()
      {
         using ISession session = _sessionFactory.OpenSession();
         using ITransaction transaction = session.BeginTransaction();
         return session.Query<StudentEntity>().ToList();
      }

      public StudentEntity GetOneById(int id)
         => GetAll().Where(w => w.Id == id).FirstOrDefault();

      public StudentEntity GetOneByName(string name)
         => GetAll().Where(w => w.FirstName == name).FirstOrDefault();

      public void SaveMany(IList<StudentEntity> entities)
      {
         using ISession session = _sessionFactory.OpenSession();
         using ITransaction transaction = session.BeginTransaction();
         foreach (var student in entities)
         {
            if (!session.Query<StudentEntity>().Any(a => a.Email == student.Email))
            {
               var course = session.Query<CourseEntity>().Where(w => w.Name == student.Course.Name).FirstOrDefault();
               if (course != null)
               {
                  student.Course = course;
               }
               session.SaveOrUpdate(student);
            }
         }
         transaction.Commit();
      }

      public void SaveOne(StudentEntity studentEntity)
      {
         using ISession session = _sessionFactory.OpenSession();
         using ITransaction transaction = session.BeginTransaction();
         if (!session.Query<StudentEntity>().Any(a => a.Email == studentEntity.Email))
         {
            var course = session.Query<CourseEntity>().Where(w => w.Name == studentEntity.Course.Name).FirstOrDefault();
            if (course != null)
            {
               studentEntity.Course = course;
            }
            session.SaveOrUpdate(studentEntity);
         }
         transaction.Commit();
      }
   }
}
