﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;

namespace otus.database
{
   class UserInput
   {
      DataManager _manager;

      public UserInput(DataManager manager)
      {
         _manager = manager;
      }
      public void InsertCourse()
      {
         Console.WriteLine("Enter course name:");
         string courseName = Console.ReadLine();
         Console.WriteLine("Enter course descriptoin");
         string courseDescription = Console.ReadLine();
         CourseEntity course = new() { Name = courseName, Description = courseDescription };
         _manager.Courses.SaveOne(course);
         Console.WriteLine(_manager.Courses.GetAll().ToString<CourseEntity>());
      }

      public void InsertLesson()
      {
         Console.WriteLine("Enter lesson name:");
         string lessonName = Console.ReadLine();
         Console.WriteLine("Enter lesson descriptoin");
         string lessonDescription = Console.ReadLine();
         Console.WriteLine("Select course id:");
         List<CourseEntity> courses = _manager.Courses.GetAll().ToList();
         courses.ForEach(f => Console.WriteLine(f));
         int courseId = GetNumericInput((p) => p >= courses.Select(s => s.Id).Min() &&
                                               p <= courses.Select(s => s.Id).Max());
         CourseEntity course = courses.Where(w => w.Id == courseId).First();
         LessonEntity lesson = new() { Course = course,  Name = lessonName, Description = lessonDescription };
         _manager.Lessons.SaveOne(lesson);
         Console.WriteLine(_manager.Lessons.GetAll().ToString<LessonEntity>());
      }

      public void InsertStudent()
      {
         Console.WriteLine("Enter student first name:");
         string studentFirstName = Console.ReadLine();
         Console.WriteLine("Enter student last name:");
         string studentLastName = Console.ReadLine();
         Console.WriteLine("Enter student email");
         string studentEmail = Console.ReadLine();
         Console.WriteLine("Enter student age");
         int studentAge = GetNumericInput((p) => p >= 0 &&
                                                 p <= 100);
         Console.WriteLine("Select course id:");
         List<CourseEntity> courses = _manager.Courses.GetAll().ToList();
         courses.ForEach(f => Console.WriteLine(f));
         int courseId = GetNumericInput((p) => p >= courses.Select(s => s.Id).Min() &&
                                               p <= courses.Select(s => s.Id).Max());
         CourseEntity course = courses.Where(w => w.Id == courseId).First();
         StudentEntity student = new() 
            {
               Course = course,
               FirstName = studentFirstName,
               LastName = studentLastName,
               Email = studentEmail,
               Age = studentAge
            };
         _manager.Students.SaveOne(student);
         Console.WriteLine(_manager.Students.GetAll().ToString<StudentEntity>());
      }

      public int GetNumericInput(Predicate<int> predicate)
      {
         int inputValue = 99;
         bool correctValue = false;
         while (!correctValue)
         {
            correctValue = Int32.TryParse(Console.ReadLine(), out inputValue) && predicate(inputValue);
            if (inputValue == 0)
            {
               Environment.Exit(0);
            }
            if (!correctValue)
            {
               Console.WriteLine("Incorrect value");
            }
         }
         return inputValue;
      }
   }
}
